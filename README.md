# OpenML dataset: weather_izmir

https://www.openml.org/d/42369

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: KEEL - [original](https://sci2s.ugr.es/keel/dataset.php?cod=78) - Date unknown  
**Please cite**:   

**Weather Izmir dataset**

This file contains the weather information of Izmir from 01/01/1994 to 31/12/1997. From given features, the goal is to predict the mean temperature.

**Attributes Information**

     1. Max_temperature real[36.7,105.0]
     2. Min_temperature real[15.8,78.6]
     3. Dewpoint real[13.6,64.4]
     4. Precipitation real[0.0,7.6]
     5. Sea_level_pressure real[29.26,30.48]
     6. Standard_pressure real[2.3,10.1]
     7. Visibility real[0.92,29.1]
     8. Wind_speed real[4.72,68.8]
     9. Max_wind_speed real[16.11,55.24]
     10. Mean_temperature real[29.4,89.9] [target]

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42369) of an [OpenML dataset](https://www.openml.org/d/42369). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42369/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42369/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42369/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

